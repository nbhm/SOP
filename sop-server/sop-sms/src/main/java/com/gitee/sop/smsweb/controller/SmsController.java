package com.gitee.sop.smsweb.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.gitee.sop.servercommon.annotation.Open;
import com.gitee.sop.smsweb.controller.param.SmsParam;
import com.gitee.sop.smsweb.message.Result;
import com.gitee.sop.smsweb.util.Signer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 签名验证通过后，到达这里进行具体的业务处理。
 *
 * @author mengzf
 */
@RestController
@RequestMapping("sms")
@Slf4j
@Api(tags = "短信接口")
public class SmsController {

    @Value("${sms.server-addr:https://api.ums86.com:9600/sms/Api}")
    private String sUrl;

    @Value("${sms.spCode:254705}")
    private String spCode;

    @Value("${weather.Secret:bdc6d5bb775146f1a698b0a8b8beb833}")
    private String weatherSecret;

    @Value("${task.Secret:ba276e7d81104366968fb09c4a35abd0}")
    private String taskSecret;

    @Value("${water.Secret:887cc5994c574ac39f649d24af76ac6f}")
    private String waterSecret;

    @Autowired
    private RedisTemplate redisTemplate;


    @Open(value = "sms.now", ignoreValidate = true)
    @RequestMapping("/now")
    public Result get() {
        return Result.ok(DateUtil.current());
    }

    @ApiOperation(value = "短信发送", notes = "一信通")
    @Open(value = "sms.send")
    @PostMapping(value = "/post/Send")
    public Result send(SmsParam smsParam) {

        HashMap<String, Object> paramMap = new HashMap<>();
        //企业编号
        paramMap.put("SpCode", spCode);
        //用户名
        paramMap.put("LoginName", StrUtil.blankToDefault(smsParam.getLoginName(), "yzqyqfkxt"));
        //接口密钥
        paramMap.put("Password", StrUtil.blankToDefault(smsParam.getPassword(), "2d7f3980238a300b75bb6f83d16ee802"));
        //短信内容, 最大402个字或字符（短信内容要求的编码为gb2312或gbk）
        paramMap.put("MessageContent", smsParam.getMessageContent());
        //手机号码(多个号码用”,”分隔)，最多100个号码
        paramMap.put("UserNumber", smsParam.getUserNumber());
        //流水号，20位数字，唯一
        paramMap.put("SerialNumber", StrUtil.blankToDefault(smsParam.getSerialNumber(), StrUtil.format("{}{}{}", DateUtil.year(new Date()), System.currentTimeMillis(), RandomUtil.randomNumbers(3))));
        //预约发送时间，格式:yyyyMMddHHmmss,如‘20090901010101’，立即发送请填空
        paramMap.put("ScheduleTime", smsParam.getScheduleTime());
        //提交时检测方式
        paramMap.put("f", smsParam.getF());

        //签名编号
        paramMap.put("signCode", smsParam.getSignCode());

        String data = HttpRequest.post(sUrl + "/Send.do")
                .charset(CharsetUtil.CHARSET_GBK)
                .form(paramMap)
                .timeout(5000)
                .execute().body();

        Map<String, String> resultMap = HttpUtil.decodeParamMap(data, CharsetUtil.CHARSET_UTF_8);

        if (resultMap.get("result").equals("0")) {
            return Result.ok(resultMap.get("taskid"), resultMap.get("description"));
        }

        return Result.failed(resultMap.get("faillist"), resultMap.get("description"));
    }

    @ApiOperation(value = "回执接口", notes = "企业方通过调用http回执接口获取短信回执信息")
    @Open(value = "sms.report")
    @PostMapping(value = "/post/report")
    public Result report(SmsParam smsParam) {

        HashMap<String, Object> paramMap = new HashMap<>();
        //企业编号
        paramMap.put("SpCode", spCode);
        //用户名
        paramMap.put("LoginName", StrUtil.blankToDefault(smsParam.getLoginName(), "yzqyqfkxt"));
        //接口密钥
        paramMap.put("Password", StrUtil.blankToDefault(smsParam.getLoginName(), "2d7f3980238a300b75bb6f83d16ee802"));

        String data = HttpRequest.post(sUrl + "/report.do")
                .charset(CharsetUtil.CHARSET_GBK)
                .form(paramMap)
                .timeout(5000)
                .execute().body();

        Map<String, String> resultMap = HttpUtil.decodeParamMap(data, CharsetUtil.CHARSET_UTF_8);

        if (resultMap.get("result").equals("0")) {
            return Result.ok(resultMap.get("out"));
        }

        return Result.failed(resultMap.get("result"));
    }

    @ApiOperation(value = "上行查询", notes = "短信上行回复查询接口")
    @Open(value = "sms.reply")
    @PostMapping(value = "/post/reply")
    public Result reply(SmsParam smsParam) {
        HashMap<String, Object> paramMap = new HashMap<>();
        //企业编号
        paramMap.put("SpCode", spCode);
        //用户名
        paramMap.put("LoginName", StrUtil.blankToDefault(smsParam.getLoginName(), "yzqyqfkxt"));
        //接口密钥
        paramMap.put("Password", StrUtil.blankToDefault(smsParam.getLoginName(), "2d7f3980238a300b75bb6f83d16ee802"));

        String data = HttpRequest.post(sUrl + "/reply.do")
                .charset(CharsetUtil.CHARSET_GBK)
                .form(paramMap)
                .timeout(5000)
                .execute().body();

        Map<String, String> resultMap = HttpUtil.decodeParamMap(data, CharsetUtil.CHARSET_UTF_8);

        if (resultMap.get("result").equals("0")) {
            return Result.ok(data);
        }

        return Result.failed(resultMap.get("result"));
    }

    @ApiOperation(value = "上行确认", notes = "上行回复内容确认接口")
    @Open(value = "sms.replyConfirm")
    @PostMapping(value = "/post/replyConfirm")
    public Result replyConfirm(SmsParam smsParam) {

        HashMap<String, Object> paramMap = new HashMap<>();
        //企业编号
        paramMap.put("SpCode", spCode);
        //用户名
        paramMap.put("LoginName", StrUtil.blankToDefault(smsParam.getLoginName(), "yzqyqfkxt"));
        //接口密钥
        paramMap.put("Password", StrUtil.blankToDefault(smsParam.getLoginName(), "2d7f3980238a300b75bb6f83d16ee802"));
        //上次查询返回的最后一条回复的id号
        paramMap.put("id", smsParam.getId());

        String data = HttpRequest.post(sUrl + "/replyConfirm.do")
                .charset(CharsetUtil.CHARSET_GBK)
                .form(paramMap)
                .timeout(5000)
                .execute().body();

        Map<String, String> resultMap = HttpUtil.decodeParamMap(data, CharsetUtil.CHARSET_UTF_8);

        if (resultMap.get("result").equals("0")) {
            return Result.ok();
        }

        return Result.failed(resultMap.get("result"));
    }


    /**
     * 获取天气预报
     * today1.实时天气，在原有功能基础上新加入风速、能见度、降雨量、气压。
     * futureDay2.未来5-7天预报。
     * futureHour3.未来逐小时预报。
     * lifeIndex5.生活指数。
     *
     * @return 适配服务端开发格式要求
     */
    @GetMapping("/weather_realtime")
    @ApiOperation(value = "天气预报", notes = "根据气象城市id获取天气预报")
    @Open(value = "sms.weather")
    public JSONObject weather(HttpServletRequest request) {
        String cityId = request.getParameter("cityId");
        log.info("用户ID{}请求:{}", request.getHeader("accountId"), cityId);

        String timestamp = request.getHeader("X-Hmac-Auth-Timestamp");
        String nonce = request.getHeader("X-Hmac-Auth-Nonce");
        String signature = request.getHeader("X-Hmac-Auth-Signature");
        String message = "GET" + "\n" + timestamp + "\n" + nonce + "\n" + "/rest/sop-sms/sms/weather_realtime";
        if (StrUtil.isNotBlank(cityId)) {
            message += "\n" + "cityId=" + cityId;
        }
        String sign = Signer.createSignature(message.getBytes(StandardCharsets.UTF_8), weatherSecret);

        if (!sign.equals(signature)) {
            return JSONUtil.createObj()
                    .putOnce("success", false)
                    .putOnce("errorCode", "403")
                    .putOnce("errorMsg", "invalid sign");
        }

        //Assert.notBlank(cityId);


        cityId = StrUtil.blankToDefault(cityId, "101210411");
        Object codeObj = redisTemplate.opsForValue().get("weather" + StrUtil.AT + cityId);
        if (codeObj != null) {
            log.info("天气未过期:{}", cityId);
            return JSONUtil.createObj().putOnce("success", true)
                    .putOnce("content", JSONUtil.createObj().putOnce("cardData", codeObj));
        }

        String result = HttpUtil.get("http://api.k780.com/?app=weather.realtime&cityId=" + cityId + "&ag=today,futureDay,lifeIndex,futureHour&appkey=51194&sign=d07a51e820ef85c67744a8f0e97035af&format=json");
        JSONObject json = JSONUtil.parseObj(result);
        if (json.getStr("success").equals("1")) {
            JSONObject resp = json.getJSONObject("result");
            redisTemplate.opsForValue().set("weather" + StrUtil.AT + cityId
                    , resp, 60 - LocalTime.now().getMinute(), TimeUnit.MINUTES);

            return JSONUtil.createObj().putOnce("success", true)
                    .putOnce("content", JSONUtil.createObj().putOnce("cardData", resp));
        } else {
            return JSONUtil.createObj().putOnce("success", false)
                    .putOnce("errorLevel", "0")
                    .putOnce("errorCode", json.getStr("success"))
                    .putOnce("errorMsg", json.getStr("msg"));

        }
    }


    /**
     * 鄞州无纸化办公
     *
     * @return 适配服务端开发格式要求
     */
    @GetMapping("/yz_task_num")
    @Open(value = "sms.task")
    @ApiOperation(value = "无纸化办公", notes = "鄞州OA")
    public JSONObject task(HttpServletRequest request) {
        String loginUserId = request.getHeader("accountId");
        String timestamp = request.getHeader("X-Hmac-Auth-Timestamp");
        String nonce = request.getHeader("X-Hmac-Auth-Nonce");
        String signature = request.getHeader("X-Hmac-Auth-Signature");
        //String message = "GET" + "\n" + timestamp + "\n" + nonce + "\n" + "/rest/sop-sms/sms/yz_task_num";
        //String sign = Signer.createSignature(message.getBytes(StandardCharsets.UTF_8), taskSecret);
        log.info("用户{}请求无纸化办公:timestamp{},nonce{},signature{}", loginUserId, timestamp, nonce, signature);

        try {
            String rest = HttpUtil.get("https://lstding.nbyz.gov.cn/yida/get_oa_tail?loginUserId=" + loginUserId);
            return JSONUtil.parseObj(rest);
        } catch (Exception e) {
            JSONObject resp = JSONUtil.createObj();
            resp.set("numList", JSONUtil.createObj()
                    .set("waitNum", "00") // 待收
                    .set("acceptNum", "00") // 待办
                    .set("meetingNum", "00"));//会议通知
            return JSONUtil.createObj().set("success", true)
                    .set("content", JSONUtil.createObj().set("cardData", resp));
        }
    }

    /**
     * 汛情监测预警
     * 作废
     *
     * @return 适配服务端开发格式要求
     */
    @GetMapping("/yz_water")
    @Open(value = "sms.water")
    @ApiOperation(value = "汛情监测", notes = "鄞州区汛情监测")
    public JSONObject water(HttpServletRequest request) {
        String timestamp = request.getHeader("X-Hmac-Auth-Timestamp");
        String nonce = request.getHeader("X-Hmac-Auth-Nonce");
        String signature = request.getHeader("X-Hmac-Auth-Signature");
        String message = "GET" + "\n" + timestamp + "\n" + nonce + "\n" + "/rest/sop-sms/sms/yz_water";
        String sign = Signer.createSignature(message.getBytes(StandardCharsets.UTF_8), waterSecret);
        log.info("用户{}请求汛情监测预警:timestamp{},nonce{},signature{},本地签名{}", request.getHeader("accountId"), timestamp, nonce, signature, sign);
        if (!sign.equals(signature)) {
            return JSONUtil.createObj()
                    .set("success", false)
                    .set("errorCode", "403")
                    .set("errorMsg", "invalid sign");
        }

        JSONObject resp = JSONUtil.createObj();
        resp.set("numList", JSONUtil.createObj()
                .set("rainNum", "01")
                .set("roadNum", "01")
                .set("bridgeNum", "02")
                .set("firstNum", "02")
                .set("secondNum", "02")
                .set("thirdNum", "02"));
        return JSONUtil.createObj().set("success", true)
                .set("content", JSONUtil.createObj().set("cardData", resp));
    }

    /**
     * 督查督办
     *
     * @return 适配服务端开发格式要求
     */
    @GetMapping("/yz_du_num")
    @Open(value = "sms.yzdu")
    @ApiOperation(value = "督查督办", notes = "鄞州区督查督办")
    public JSONObject yzdu(HttpServletRequest request) {
        String loginUserId = request.getHeader("accountId");
        String timestamp = request.getHeader("X-Hmac-Auth-Timestamp");
        String nonce = request.getHeader("X-Hmac-Auth-Nonce");
        String signature = request.getHeader("X-Hmac-Auth-Signature");
        //String message = "GET" + "\n" + timestamp + "\n" + nonce + "\n" + "/rest/sop-sms/sms/yz_du_num";
        //String sign = Signer.createSignature(message.getBytes(StandardCharsets.UTF_8), waterSecret);
        log.info("用户{}请求督查督办:timestamp{},nonce{},signature{}", loginUserId, timestamp, nonce, signature);
        //log.info("用户{}请求督查督办:timestamp{},nonce{},signature{},本地签名{}", request.getHeader("accountId"), timestamp, nonce, signature, sign);
//        if(!sign.equals(signature)){
//            return JSONUtil.createObj()
//                    .set("success", false)
//                    .set("errorCode", "403")
//                    .set("errorMsg", "invalid sign");
//        }

        try {
            String rest = HttpUtil.get("https://ydc.nbyz.cn/yzdu/api/get_mainJob_tail?loginUserId=" + loginUserId);
            return JSONUtil.parseObj(rest);
        } catch (Exception e) {
            JSONObject resp = JSONUtil.createObj();
            resp.set("numList", JSONUtil.createObj()
                    .set("mainJob", "暂无法获取")
                    .set("lastDay", "--")
                    .set("green", "00")
                    .set("red", "00")
                    .set("severedelay", "00")
                    .set("handlerate", "00")
                    .set("overhandlerate", "00")
                    .set("backrate", "00"));
            return JSONUtil.createObj().set("success", true)
                    .set("content", JSONUtil.createObj().set("cardData", resp));
        }


    }

}
