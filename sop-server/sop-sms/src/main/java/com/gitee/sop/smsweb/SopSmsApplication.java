package com.gitee.sop.smsweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SopSmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SopSmsApplication.class, args);
    }
}
