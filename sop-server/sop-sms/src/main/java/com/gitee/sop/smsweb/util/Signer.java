package com.gitee.sop.smsweb.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Signer {

    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String UTF_8 = "UTF-8";

    public static String createSignature(byte[] message, String secretKey) {
        byte[] digest = calculateDigest(message, secretKey);
        return Base64.encodeBase64String(digest);
    }

    private static byte[] calculateDigest(byte[] message, String secretKey) {
        try {
            byte[] secretKeyBytes = secretKey.getBytes(UTF_8);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, HMAC_SHA256);
            Mac mac = Mac.getInstance(HMAC_SHA256);
            mac.init(secretKeySpec);
            mac.update(message);
            return mac.doFinal();

        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Invalid character encoding: " + UTF_8, e);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Invalid MAC algorithm: " + HMAC_SHA256, e);
        } catch (InvalidKeyException e) {
            throw new IllegalStateException("Invalid MAC secret key", e);
        }
    }


    public static void main(String[] args) throws UnsupportedEncodingException {
        String timestamp = "2022-11-12T14:48:54.376+08:00";
        String nonce = "1668235734376692";
        String signature = "K1GwsiR1BliMnHeh+VzV057bOAOWrB/drcM7tQZIKF4=";
        String message = "GET" + "\n" + timestamp + "\n" + nonce + "\n" + "/rest/sop-sms/sms/yz_du_num";
        String sign = Signer.createSignature(message.getBytes("UTF-8"), "887cc5994c574ac39f649d24af76ac6f");
        Console.log(DateUtil.lastMonth());
    }


}