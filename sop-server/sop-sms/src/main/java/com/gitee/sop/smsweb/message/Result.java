/*
 *
 *      Copyright (c) 2018-2025, hmkj All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the pig4cloud.com developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: hmkj
 *
 */

package com.gitee.sop.smsweb.message;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @param <T>
 * @author hmkj
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "响应信息主体")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @ApiModelProperty(value = "返回标记：成功标记=0，失败标记=1")
    private int code;

    @Getter
    @Setter
    @ApiModelProperty(value = "返回信息")
    private String msg;

    @Getter
    @Setter
    @ApiModelProperty(value = "数据")
    private T data;

    public static <T> Result<T> ok() {
        return restResult(null, 0, null);
    }

    public static <T> Result<T> ok(T data) {
        return restResult(data, 0, null);
    }

    public static <T> Result<T> ok(T data, String msg) {
        return restResult(data, 0, msg);
    }

    public static <T> Result<T> failed() {
        return restResult(null, 1, null);
    }

    public static <T> Result<T> failed(String msg) {
        return restResult(null, 1, msg);
    }

    public static <T> Result<T> failed(T data) {
        return restResult(data, 1, null);
    }

    public static <T> Result<T> failed(T data, String msg) {
        return restResult(data, 1, msg);
    }

    private static <T> Result<T> restResult(T data, int code, String msg) {
        Result<T> apiResult = new Result<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

}
