package com.gitee.sop.smsweb.controller.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class SmsParam {

    @ApiModelProperty(value = "最后一条回复的id号", example = "2001062", position = 1)
    private String id;


    @ApiModelProperty(value = "用户名", required = true, example = "xx", position = 2)
    private String LoginName;

    @ApiModelProperty(value = "接口密钥", example = "xx", position = 3)
    private String Password;

    @NotBlank(message = "MessageContent不能为空")
    @ApiModelProperty(value = "短信内容", example = "xx", position = 4)
    @Length(max = 400, message = "MessageContent长度不能超过400")
    private String MessageContent;

    @NotBlank(message = "手机号码不能为空")
    @ApiModelProperty(value = "手机号码", example = "号码1;号码2", position = 5)
    private String UserNumber;


    @ApiModelProperty(value = "流水号", example = "20位数字", position = 6)
    private String SerialNumber;


    @ApiModelProperty(value = "预约发送时间", example = "20090901010101’", position = 7)
    private String ScheduleTime;

    @ApiModelProperty(value = "提交时检测方式", example = "xx", position = 8)
    private String f;

    @ApiModelProperty(value = "签名编号", example = "6a111d8c-326d-416a-aefe-d1c8422cda34", position = 9)
    private String signCode;


}