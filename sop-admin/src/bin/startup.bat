@echo off
rem ======================================================================
rem windows startup script
rem
rem author: geekidea
rem date: 2021-11-2
rem ======================================================================

rem Open in a browser
rem start "" "http://localhost:8080/example/hello?name=123"

rem startup jar
title @project.build.finalName@

java -server -Xms256m -Xmx256m -Xmn512m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=256m -XX:-OmitStackTraceInFastThrow -Dfile.encoding=UTF-8 -jar ../boot/@project.build.finalName@.jar --spring.config.location=../config/

pause
